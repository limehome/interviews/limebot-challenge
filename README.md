Limehome Coding Challenge
=========================

Welcome to the limehome coding challenge for the limebot team. Here is what we are looking for in your answers:
* Project layout: We want to see how you structure your code. Where do the data loading scripts go? How do i start the api? Assume that this is a project for a production code base. *Please do not just send an ipython notebook.* 
* Quality of Code: We want to see that you can deliver a clean solution. This includes everything from variable names, to not having commented out code checked in, to having easy to understand steps on how to run your solution.
* Scalability: We don't want you to tune it to squeeze out the last milliseconds in runtime, but it should be good enough to be used by real users.
* For most of the questions there is no right or wrong answer, but make sure to always include pro / cons in your solutions.

# General notes
If you send text, a normal text file is sufficient. PDF is also okay, please do not send word or open office files as the reviewer might not have access to them.


# TLC Trip Record Data
The goal for this task is to analyse the NYC taxi trip data. Please choose a database (or database like) system of your choice. This can run locally on your laptop or be a hosted version on the cloud. Please sent us all the ressources you used while creating your data store. This includes schemas, setup scripts, data loading scripts and of course the queries themselves.
To help you better decide, here are some requirements:
* Will be used by analysts to create charts and by data scientists as in input for their model generation
* The schema of the data might change over time

1. Download the dataset from [taxi_rides](https://tech-interviews-challenge-assets.s3.eu-central-1.amazonaws.com/yellow_tripdata_2020.csv). Load the data set into your system. 
2. Now it is time to run some queries on your newly created data set. Please share with us the queries for the following questions, as well as their execution times.
    * Count the number of trips where the total amount is less than 5$.
    * At what hour of the day do people spent the most money on taxi rides?
    * What is average percentage of the tip?
3. Lets increase the the amount of data. For this just rerun your import script for 10 times. Rerun your queries and share their execution time with us.

# API
Since other teams inside our company are interested in the data, we want to give them programmtic access through an API. Please write your solution in python, you are free to use as many libraries as you want:
1. Return the number of trips, where to the total amount is less than N. N is a parameter, which can be passed by the consumer of the API.
